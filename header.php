<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">

<meta name="viewport" content="width=360, maximum-scale=1, user-scalable=no" />
<meta name="viewport" content="width=device-width, maximum-scale=1, user-scalable=no" />

<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" />

<?php wp_head(); ?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" />
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Work+Sans:700,300,400|Source+Sans+Pro:400,700|Roboto:700,300|Roboto+Mono:400,700|Material+Icons" />

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/inc.scssphp.php/style.scss" />

</head>

<body <?php body_class(); ?>>

<div id="wrapper" class="hfeed container-fluid">

<header id="header" class="row">
	<div class="container">
		<div class="row">

			<div id="logo" class="col-8 col-md-3 ">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/openport-logo.svg"></a>
			</div>

			<div id="menu-toggle" class="col hidden-lg-up text-right">
			<i class="material-icons">menu</i>
			</div>
			
			
			<div id="nav-menu-container"  class="col-12 col-lg-9 hidden-md-down"  >
			<?php include 'inc.menu.php';?>
			
			</div>
			

		</div>
	</div>
</header>
  
  

<div id="main-content" class="row">

		