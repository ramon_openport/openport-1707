<?php

//DISABLE ADMIN BAR
add_filter('show_admin_bar', '__return_false');

// REMOVE EMOJI ICONS
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');


/**
 * Remove empty paragraphs created by wpautop()
 * @author Ryan Hamilton
 * @link https://gist.github.com/Fantikerz/5557617
 */
function remove_empty_p( $content ) {
    $content = force_balance_tags( $content );
    $content = preg_replace( '#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content );
    $content = preg_replace( '~\s?<p>(\s|&nbsp;)+</p>\s?~', '', $content );
    return $content;
}
add_filter('the_content', 'remove_empty_p', 20, 1);

function add_theme_scripts() {
wp_deregister_script('jquery');
wp_register_script( 'jquery', 'https://cdn.jsdelivr.net/jquery/1.12.4/jquery.min.js', array(), '1.12.4' );
wp_enqueue_script('jquery');
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' ); 

?>